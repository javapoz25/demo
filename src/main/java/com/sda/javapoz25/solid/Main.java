package com.sda.javapoz25.solid;

public class Main {
    // S O L I D - zasady czystego kodu. Kod który łatwo się utrzymuje, jest tani w utrzymaniu, zrozumiały dla każego,
    //              łatwy w rozwijaniu.
    //
    // S - Single Responsibility - Pojedyncza odpowiedzialnosc
    //          - jedna klasa/metoda powinna odpowiadać za jedną rzecz
    //
    //          - klasa która odpowiada za zapis do pliku (konfiguracji)
    //              - jeśli konfiguracja z jakiegoś względu nie działa, to wiem gdzie szukać
    //
    //          - jeśli podzielę implementację i stworzę klasę:
    //              - która zapisuje do pliku, wczytuje z pliku, generuje konfiguracje,
    //              - odpowiada za zapis do bazy danych
    //              - podczas modyfikacji istnieje duże ryzyko, że naprawiając jedną rzecz uszkodzę inną
    //
    //  O - Open close - Klasa powinna być otwarta na rozszerzenia i zamknięta na modyfikacje
    //          - jeśli napisałem fragment kodu, to w idealnym scenariuszu nie powinienem mieć powodu do edycji,
    //              a wyłącznie do rozszerzania klasy.
    //          - jeśli modyfikuje klase, to dlatego ze muszę coś naprawić, a nie rozszerzyć/dodać
    //
    //  L - Liskov Substitution - Po lewej stronie równania zastosuj dowolny typ nadrzędny który dalej pozwoli
    //              realizować Twoją funkcjonalność.
    //
    //  I - Interface segregation - segreguj interfejsy, twórz możliwie najprostsze interfejsy (np. z jedną metodą)
    //
    //  D - do omówienia przy springu, pokazanie warstw wykonania i zaleznosci (na module Dependency Injection)
}
