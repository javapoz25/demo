package com.sda.javapoz25.solid.i.good;

public interface IZakluczalny {
    void zaklucz();
    void odklucz();
}
