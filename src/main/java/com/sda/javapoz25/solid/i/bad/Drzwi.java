package com.sda.javapoz25.solid.i.bad;

public class Drzwi implements IDrzwi {
    private boolean otwarte;
    private boolean zakluczone;

    @Override
    public void otworz() {
        otwarte = true;
    }

    @Override
    public void zamknij() {
        otwarte = false;
    }

    @Override
    public void zaklucz() {
        otwarte = false;
        zakluczone = true;
    }

    @Override
    public void odklucz() {
        zakluczone = false;
    }
}
