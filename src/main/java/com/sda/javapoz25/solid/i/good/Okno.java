package com.sda.javapoz25.solid.i.good;

public class Okno implements IOtwieralny, IZamykalny{
    private boolean otwarte;

    @Override
    public void otworz() {
        otwarte = true;
    }

    @Override
    public void zamknij() {
        otwarte = false;
    }
}
