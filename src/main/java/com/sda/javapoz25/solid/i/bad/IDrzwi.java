package com.sda.javapoz25.solid.i.bad;

public interface IDrzwi {
    void otworz();
    void zamknij();
    void zaklucz();
    void odklucz();
}
