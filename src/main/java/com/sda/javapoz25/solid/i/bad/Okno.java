package com.sda.javapoz25.solid.i.bad;

public class Okno implements IDrzwi {
    private boolean otwarte;

    @Override
    public void otworz() {
        otwarte = true;
    }

    @Override
    public void zamknij() {
        otwarte = false;
    }

    // dwie poniższe metody zostały zaimplementowane ponieważ są częścią interfejsu,
    // który postanowiliśmy zaimplementować
    @Override
    public void zaklucz() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void odklucz() {
        throw new UnsupportedOperationException();
    }
}
