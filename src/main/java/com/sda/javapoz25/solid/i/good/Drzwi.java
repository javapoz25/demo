package com.sda.javapoz25.solid.i.good;

public class Drzwi implements IZamykalny, IZakluczalny, IOtwieralny{
    private boolean otwarte;
    private boolean zakluczone;

    @Override
    public void otworz() {
        otwarte = true;
    }

    @Override
    public void zamknij() {
        otwarte = false;
    }

    @Override
    public void zaklucz() {
        otwarte = false;
        zakluczone = true;
    }

    @Override
    public void odklucz() {
        zakluczone = false;
    }
}
