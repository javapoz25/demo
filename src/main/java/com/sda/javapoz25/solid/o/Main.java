package com.sda.javapoz25.solid.o;

public class Main {
    public static void main(String[] args) {
        Bohater bohater = new Bohater("Zbyszko");

        bohater.setStrategiaWalki(new WalkaMieczem());
        bohater.walcz(); //Walczę mieczem!

        bohater.setStrategiaWalki(new WalkaŁukiem());
        bohater.walcz(); //Walczę łukiem! Pew pew!

        bohater.setStrategiaWalki(new WalkaOwcą());
        bohater.walcz(); //Podkładam owcę z prochem!


    }
}
