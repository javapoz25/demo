package com.sda.javapoz25.solid.o;

import lombok.Setter;

// Gra w której bohater (postać) może walczyć ze smokiem.
//  - chcemy stworzyć mechanizm walki na 3 sposoby
//  - chcemy zachować zasadę open/close
//  - (wzorzec projektowy strategy) - wzorzec projektowy to praktyczny sposób na rozwiązanie problemu
@Setter
public class Bohater {
    private String imie;
    private IStrategiaWalki strategiaWalki;

    public Bohater(String imie) {
        this.imie = imie;
    }

    public void walcz(){
        strategiaWalki.walcz();
    }
}
