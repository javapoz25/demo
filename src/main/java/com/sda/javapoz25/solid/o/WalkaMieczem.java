package com.sda.javapoz25.solid.o;

public class WalkaMieczem implements IStrategiaWalki {
    @Override
    public void walcz() {
        System.out.println("Walczę mieczem!");
    }
}
