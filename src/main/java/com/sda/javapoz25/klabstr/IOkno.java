package com.sda.javapoz25.klabstr;

public interface IOkno {
    public abstract void otworz();

    public default void zamknij(){
        System.out.println("Zamykam");
    }

    // SOLID - zasady czystego kodu
}
