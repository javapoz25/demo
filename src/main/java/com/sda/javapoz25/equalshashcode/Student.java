package com.sda.javapoz25.equalshashcode;

import lombok.Data;

import java.util.Objects;

@Data
public class Student {
    private String imie;
    private String nazwisko;

    private String nrIndeksu;

    // alt + insert -> generate -> equals and hashcode
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;                                 // jeśli adres w pamięci się zgadza (true)
        if (o == null || getClass() != o.getClass()) return false;  // jeśli nie są tej samej klasy (false)
        Student student = (Student) o;                              // rzutuj
        return nrIndeksu.equals(student.nrIndeksu);                 // porónwaj pole/a
    }

    // hashcode to powinien być ukikalny kod generowany na podstawie pól
    @Override
    public int hashCode() {             // domyślnie hashcode = adres w pamięci
        return Objects.hash(nrIndeksu);
    }

    // jeśli metoda equals między obiektem o1 i o2 zwróci true
    // to hashcode wygenerowany na podstawie obiektu o1 i obiektu o2 są sobie równe

}
