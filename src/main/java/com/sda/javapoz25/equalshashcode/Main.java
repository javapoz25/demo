package com.sda.javapoz25.equalshashcode;

public class Main {

    public static void main(String[] args) {
        Student st = new Student(); // obiekt 1 - ma inny adres w pamięci niż
        st.setNrIndeksu("123");

        Student st2 = new Student(); // obiekt 2
        st2.setNrIndeksu("123");

        System.out.println("Są równe? : " + (st2.equals(st)));
    }
}
