package com.sda.javapoz25.collectionBenchmark;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // array = 7ms
        // linked = 7550 ms
        List<Integer> integers = new LinkedList<>();
        for (int i = 0; i < 1000000; i++) {
            integers.add(i);
        }

        // listy    - ArrayList, LinkedList
        //              -- benchmark
        // złożoność obl. - ilość instrukcji/cyklów procesora (uzależniona od rozmiaru danych) do wykonania danej czynności


        // set      - HashSet, LinkedHashSet, TreeSet
        // map      - HashMap, LinkedHashMap, TreeMap

        // Linked - zachowywana jest kolejność wstawiania elementów
        // Tree   - sortuje elementy

        porownajCzasWykonania(integers);
    }


    public static void porownajCzasWykonania(List<Integer> lista) {
        Long czasPoczatek = System.currentTimeMillis();

        for (int i = 0; i < lista.size(); i++) {
            int a = lista.get(i);
        }

        Long czasKoniec = System.currentTimeMillis();
        System.out.println("Czas: " + (czasKoniec - czasPoczatek) + "ms");
    }

}
