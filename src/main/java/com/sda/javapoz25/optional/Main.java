package com.sda.javapoz25.optional;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        KlasaObliczeniowa klasaObliczeniowa = new KlasaObliczeniowa();
        Optional<Integer> wynikOptional = klasaObliczeniowa.podziel(20, 0); // null

        // rolą optionala jest przypominanie
        if (wynikOptional.isPresent()) { // sprawdź czy jest wynik
            Integer wynik = wynikOptional.get(); // rozpakuj wynik z pudełka

            System.out.println("Wynik *100 =" + (wynik.doubleValue() * 100)); // NullPointerException
        }
    }

    /**
     *          Integer wynik = klasaObliczeniowa.podziel(20, 0); // null
     *
     *          if (wynik != null) {
     *              System.out.println("Wynik *100 =" + (wynik.doubleValue() * 100)); // NullPointerException
     *          }
     */
}
