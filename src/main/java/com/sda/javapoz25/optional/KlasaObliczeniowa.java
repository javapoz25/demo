package com.sda.javapoz25.optional;

import java.util.Optional;
import java.util.OptionalDouble;

public class KlasaObliczeniowa {
    /**
     * UWAGA wartość zwracana może wynosić null - nic.
     *
     * @param a
     * @param b
     * @return
     */
    // Ponieważ wiem, że wynik może nie zostać naleziony, to zamiast zwracać w tym miejscu null, zwracam pusty Optional
    public Optional<Integer> podziel(int a, int b) {
        if (b == 0) {
//            throw new UnsupportedOperationException("Nie dziel przez 0!");
//            return null;
            return Optional.empty(); // nullptr / lub Optional.empty() - puste pudełko (brak wartości zwróconej)
        }
        return Optional.of(a / b); // wstawiamy w pudełko wynik
    }
}
