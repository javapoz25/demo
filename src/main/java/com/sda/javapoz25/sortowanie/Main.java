package com.sda.javapoz25.sortowanie;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Person> list = new ArrayList<>();

        list.addAll(Arrays.asList(
                new Person("Marek", 10),
                new Person("Ania", 15),
                new Person("Arek", 7),
                new Person("Waldek", 9)
        ));

        // mamy dwie możliwości sortowania obiektów:
        //          1- obiekty same potrafią się do siebie porównać (Comparable)
        //          2- mamy obiekt porównujący (Comparator)
        // Opcja 1:
        Collections.sort(list);

        // Opcja 2:
//        Collections.sort(list, new PersonComparator());
//        Collections.sort(list, (o1, o2) -> Integer.compare(o2.getWiek(), o1.getWiek()));
//        Collections.sort(list, new Comparator<Person>() { // klasa anonimowa - klasa która implementuje interfejs Comparator
//            @Override
//            public int compare(Person o1, Person o2) {
//                return Integer.compare(o2.getWiek(), o1.getWiek());
//            }
//        });

        // żeby każdy wiersz wypisał się w nowej linii, użyje stream
//        for (Person person : list) {
//            System.out.println(person);
//        }
        list.stream() // dla każdego obiektu typu Person, o nazwie person
                .forEach(person -> {
                    System.out.println(person);
                });

        list.forEach(System.out::println);
    }
}
