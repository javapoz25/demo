package com.sda.javapoz25.sortowanie;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person implements Comparable<Person>{
    private String imie;
    private int wiek;

    @Override
    public int compareTo(Person other) {
        // porównanie obiektu "this" z obiektem "other"
//        return Integer.compare(other.getWiek(), this.getWiek());
        return this.getImie().compareTo(other.getImie());
    }
}
