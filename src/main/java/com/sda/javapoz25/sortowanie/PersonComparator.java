package com.sda.javapoz25.sortowanie;

import java.util.Comparator;

public class PersonComparator implements Comparator<Person> {
    // zwracamy jedną z wartości:
    // jeśli o1>o2  = -1
    // jeśli o1=o2  = 0
    // jeśli o1<o2  = 1
    @Override
    public int compare(Person o1, Person o2) {
        return Integer.compare(o2.getWiek(), o1.getWiek());

//        if (o1.getWiek() > o2.getWiek()) {
//            return 1;
//        } else if (o2.getWiek() > o1.getWiek()) {
//            return -1;
//        }
//        return 0;
    }
}
