package com.sda.javapoz25.lombok;

import lombok.*;

//@Getter
//@Setter
//@ToString
@EqualsAndHashCode
//@RequiredArgsConstructor   //kiedy jest generowany z parametrem?
// @Data to  ^^ wszystko powyższe
@Getter
@Setter
@AllArgsConstructor
public class Pair {
    private int a;
    private int b;

}
