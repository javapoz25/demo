package com.sda.javapoz25.lombok;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Pair p12 = new Pair(1, 2);
        Pair p21 = new Pair(2, 1);
        Pair p111 = new Pair(1,1 );
        Pair p112 = new Pair(1,1 );
        Pair p22 = new Pair(2,2 );

        // jeśli metoda equals porównująca obiekty zwróci true
        // to metoda hashcode musi zwrócić tą samą wartość
        Set<Pair> set = new HashSet<>();
        set.add(p111);
        set.add(p112);
        set.add(p12);
        set.add(p22);
        set.add(p21);

        System.out.println("Rozmiar: " + set.size());

    }
}
