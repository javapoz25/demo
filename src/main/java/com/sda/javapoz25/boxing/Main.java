package com.sda.javapoz25.boxing;

public class Main {
    public static void main(String[] args) {
        int wartosc = 5;

                    //         = new Integer(wartosc); <-boxing (box = pudełko) <= pakowanie do pudełka
        Integer wartoscInteger = wartosc; // boxing - > opakowanie typu prymitywnego w obiektowy (bez konieczności rzutowania)

        metoda(wartosc); // boxing - > opakowanie typu prymitywnego w obiektowy
    }

    public static void metoda(Integer wartoscLiczbowa){
        System.out.println(wartoscLiczbowa);
    }
}
