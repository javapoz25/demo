package com.sda.javapoz25.polimorfizm;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        metodaWypisz();
//        metodaWypisz("Tekst do wypisania");

        Ptak p = new Kukulka();
        p.spiewaj();    //??  - ku ku

        // Po lewej interfejs, po prawej klasa końcowa
        List<Ptak> ptakList = new ArrayList<>();

        ptakList.add(new Kukulka());
        ptakList.add(new Bocian());
        ptakList.add(new Sowa());
        ptakList.add(new Ptak());

        System.out.println(ptakList);
    }

    // wielopostaciowość metod
    private static void metodaWypisz(String coWypisac) {
        System.out.println(coWypisac);
    }

    private static void metodaWypisz() {
        System.out.println("Nic");
    }
}
